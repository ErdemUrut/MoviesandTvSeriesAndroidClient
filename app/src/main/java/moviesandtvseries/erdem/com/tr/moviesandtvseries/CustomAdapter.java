package moviesandtvseries.erdem.com.tr.moviesandtvseries;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater = null;
    JSONArray jsonArray;
    List<String> checkedMoviesIds = new ArrayList<>();
    List<String> moviesIds = new ArrayList<>();

    public CustomAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return jsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView titleYearTextView, ratingTextView;
        ImageView img;
        CheckBox checkBoxMovies;
    }

    public List<String> getCheckedList() {
        return checkedMoviesIds;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_item, null);

        holder.titleYearTextView = (TextView) rowView.findViewById(R.id.titleyearTextView);
        holder.ratingTextView = (TextView) rowView.findViewById(R.id.ratingTextView);
        holder.img = (ImageView) rowView.findViewById(R.id.seriesImageView);
        holder.checkBoxMovies = (CheckBox) rowView.findViewById(R.id.checkBoxMovies);


        try {
            JSONObject jsonObject2 = jsonArray.getJSONObject(position);
            String movies_id = jsonObject2.getString("movies_id");
            moviesIds.add(movies_id);
            String year = jsonObject2.getString("year");
            String rating = jsonObject2.getString("rating");
            String ranking = jsonObject2.getString("ranking");
            String title = jsonObject2.getString("title");
            String url = jsonObject2.getString("url");

            holder.titleYearTextView.setText(title + " \n \t\t\t\t\t\t" + year);
            holder.ratingTextView.setText(rating);
            holder.checkBoxMovies.setChecked(false);
            Picasso.with(context).load(url).into(holder.img);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked ", Toast.LENGTH_LONG).show();
            }
        });

        holder.checkBoxMovies.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                checkedMoviesIds.add(moviesIds.get(position));
            }
        });
        return rowView;
    }

}