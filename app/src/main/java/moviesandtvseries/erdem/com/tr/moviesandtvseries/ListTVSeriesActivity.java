package moviesandtvseries.erdem.com.tr.moviesandtvseries;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import moviesandtvseries.erdem.com.tr.moviesandtvseries.helpers.JSONParser;

public class ListTVSeriesActivity extends AppCompatActivity {

    ProgressDialog progress;
    ListView lv;
    Button addFavoriteView;


    JSONObject jsonObject = null;
    JSONParser jsonParser = new JSONParser();
    JSONArray jsonArray = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_tvseries);
        addFavoriteView = (Button) findViewById(R.id.add_favorite);
        addFavoriteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TvSeriesListing().execute();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_tvseries, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class TvSeriesListing extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(ListTVSeriesActivity.this);
            progress.setMax(100);
            progress.setMessage("Lütfen bekleyiniz.");
            progress.setTitle("Liste dolduruluyor .");
            progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progress.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.dismiss();
            try {
                jsonArray = jsonObject.getJSONArray("tvseries");
                lv = (ListView) findViewById(R.id.listView);
                lv.setAdapter(new CustomAdapter2(ListTVSeriesActivity.this, jsonArray));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            jsonObject = jsonParser.makeHttpRequest("http://192.168.2.73:8080/service/tvseries", "GET", new HashMap<String, String>());
            return null;
        }
    }

}

