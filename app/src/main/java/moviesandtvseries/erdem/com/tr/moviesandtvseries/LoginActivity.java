package moviesandtvseries.erdem.com.tr.moviesandtvseries;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import moviesandtvseries.erdem.com.tr.moviesandtvseries.helpers.JSONParser;

public class LoginActivity extends AppCompatActivity {

    DatabaseHelper helper = new DatabaseHelper(this);

    @Bind(R.id.userEditText)
    EditText userEditText;
    @Bind(R.id.passwordEditText)
    EditText passwordEditText;
    @Bind(R.id.loginButton)
    Button loginButton;
    @Bind(R.id.registerTextView)
    TextView registerTextView;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userEditText.getText().length() <= 0) {
                    Toast.makeText(LoginActivity.this, "Lütfen e-mail adresinizi giriniz.", Toast.LENGTH_LONG).show();
                    userEditText.setError("Doldurulması gerekli alan.");
                } else if (passwordEditText.getText().length() <= 0) {
                    Toast.makeText(LoginActivity.this, "Lütfen ´şifrenizi giriniz.", Toast.LENGTH_LONG).show();
                    passwordEditText.setError("Doldurulması gerekli alan.");
                } else {
                    progress = new ProgressDialog(LoginActivity.this);
                    progress.setMax(100);
                    progress.setMessage("Lütfen bekleyiniz.");
                    progress.setTitle("Giriş yapılıyor .");
                    progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progress.show();
                    new Login().execute();
                }

            }


        });

        registerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    Users u = new Users();
    JSONObject jsonObject = null;
    Boolean success = null;
    String message = "";
    String user_details = "";
    JSONObject jsonObjectUserDetails = null;

    class Login extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            JSONParser jsonParser = new JSONParser();
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("email", u.getEmail());
            parameters.put("password", u.getPassword());

            jsonObject = jsonParser.makeHttpRequest("http://dizivefilmtakipws.elasticbeanstalk.com/service/users/login", "POST", parameters);

            return null;

        }


        String email = userEditText.getText().toString();
        String pass = passwordEditText.getText().toString();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            u.setEmail(email);
            u.setPassword(pass);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.dismiss();

            try {
                success = jsonObject.getBoolean("success");
                jsonObjectUserDetails = jsonObject.getJSONObject("user_details");
                String lastName = jsonObjectUserDetails.getString("last_name");
                String firstName = jsonObjectUserDetails.getString("first_name");
                Log.d("LoginActivity", lastName + firstName);
                message = jsonObject.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (success)

            {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            } else

            {
                Toast.makeText(LoginActivity.this, "Böyle bir kullanıcı yoktur.", Toast.LENGTH_LONG).show();
            }
        }
    }


}
