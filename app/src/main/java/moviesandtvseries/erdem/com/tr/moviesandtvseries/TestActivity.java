package moviesandtvseries.erdem.com.tr.moviesandtvseries;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TestActivity extends AppCompatActivity {

    @Bind(R.id.testTextView)
    TextView testTextView;
    @Bind(R.id.testButton)
    Button testButton;

        String json="{\n" +
                "   \"data\": [\n" +
                "      {\n" +
                "         \"id\": \"X999_Y999\",\n" +
                "         \"from\": {\n" +
                "            \"name\": \"Tom Brady\", \"id\": \"X12\"\n" +
                "         },\n" +
                "         \"message\": \"Looking forward to 2010!\",\n" +
                "         \"actions\": [\n" +
                "            {\n" +
                "               \"name\": \"Comment\",\n" +
                "               \"link\": \"http://www.facebook.com/X999/posts/Y999\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\": \"Like\",\n" +
                "               \"link\": \"http://www.facebook.com/X999/posts/Y999\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"type\": \"status\",\n" +
                "         \"created_time\": \"2010-08-02T21:27:44+0000\",\n" +
                "         \"updated_time\": \"2010-08-02T21:27:44+0000\"\n" +
                "      },\n" +
                "      {\n" +
                "         \"id\": \"X998_Y998\",\n" +
                "         \"from\": {\n" +
                "            \"name\": \"Peyton Manning\", \"id\": \"X18\"\n" +
                "         },\n" +
                "         \"message\": \"Where's my contract?\",\n" +
                "         \"actions\": [\n" +
                "            {\n" +
                "               \"name\": \"Comment\",\n" +
                "               \"link\": \"http://www.facebook.com/X998/posts/Y998\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\": \"Like\",\n" +
                "               \"link\": \"http://www.facebook.com/X998/posts/Y998\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"type\": \"status\",\n" +
                "         \"created_time\": \"2010-08-02T21:27:44+0000\",\n" +
                "         \"updated_time\": \"2010-08-02T21:27:44+0000\"\n" +
                "      }\n" +
                "   ]\n" +
                "}";

    JSONArray jsonArray = new JSONArray();
    JSONObject jsonObject;
    JSONObject jsonObject2 = new JSONObject();
    JSONObject jsonObject3 = new JSONObject();
    JSONObject jsonObject4 = new JSONObject();
    JSONObject jsonObject5 = new JSONObject();
    JSONArray jsonArray2 = new JSONArray();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);


        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    jsonObject = new JSONObject(json);

                        jsonArray=jsonObject.getJSONArray("data");
                        jsonObject2=jsonArray.getJSONObject(0);
                        jsonArray2=jsonObject2.getJSONArray("actions");
                        jsonObject3=jsonArray2.getJSONObject(1);
                        String test=jsonObject3.getString("link");
                    testTextView.setText(test);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
