package moviesandtvseries.erdem.com.tr.moviesandtvseries;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import moviesandtvseries.erdem.com.tr.moviesandtvseries.helpers.JSONParser;

public class ChangePasswordActivity extends AppCompatActivity {

    @Bind(R.id.emailEditText)
    EditText emailEditText;
    @Bind(R.id.passwordOldEditText)
    EditText passwordOldEditText;
    @Bind(R.id.passwordNewEditText)
    EditText passwordNewEditText;
    @Bind(R.id.saveButton)
    Button saveButton;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emailEditText.getText().length() <= 0) {
                    Toast.makeText(ChangePasswordActivity.this, "Lütfen e-mail adresinizi giriniz.", Toast.LENGTH_LONG).show();
                    emailEditText.setError("Doldurulması gerekli alan.");
                } else if (passwordOldEditText.getText().length() <= 0) {
                    Toast.makeText(ChangePasswordActivity.this, "Lütfen soyadınızı giriniz.", Toast.LENGTH_LONG).show();
                    passwordOldEditText.setError("Doldurulması gerekli alan.");
                } else if (passwordNewEditText.getText().length() <= 0) {
                    Toast.makeText(ChangePasswordActivity.this, "Lütfen e-mail adresinizi giriniz.", Toast.LENGTH_LONG).show();
                    passwordNewEditText.setError("Doldurulması gerekli alan.");
                } else {
                    progress = new ProgressDialog(ChangePasswordActivity.this);
                    progress.setMax(100);
                    progress.setMessage("Lütfen bekleyiniz.");
                    progress.setTitle("Kaydınız yapılıyor .");
                    progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progress.show();
                    new ChangePassword().execute();
                }

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class ChangePassword extends AsyncTask<Void, Void, Void> {

        String strEmail = emailEditText.getText().toString();
        String strOldPassword = passwordOldEditText.getText().toString();
        String strNewPassword = passwordNewEditText.getText().toString();


        JSONObject jsonObject = null;
        Boolean success = null;
        String message = "";
        Users u = new Users();

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.dismiss();

            try {
                success = jsonObject.getBoolean("success");
                message = jsonObject.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            if (success) {
                Toast.makeText(ChangePasswordActivity.this, message, Toast.LENGTH_LONG).show();
                startActivity(new Intent(ChangePasswordActivity.this, MainActivity.class));
            } else {
                Toast.makeText(ChangePasswordActivity.this, message, Toast.LENGTH_LONG).show();
            }

        }

        @Override
        protected Void doInBackground(Void... params) {

            JSONParser jsonParser = new JSONParser();
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("email", strEmail);
            parameters.put("old_password", strOldPassword);
            parameters.put("new_password", strNewPassword);

            jsonObject = jsonParser.makeHttpRequest("http://dizivefilmtakipws.elasticbeanstalk.com/service/users/change-password", "POST", parameters);


            return null;
        }
    }
}
