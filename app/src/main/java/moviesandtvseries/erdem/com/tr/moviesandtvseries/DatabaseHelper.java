package moviesandtvseries.erdem.com.tr.moviesandtvseries;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Erdem on 23.10.2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "dizivefilmtakip.db";
    private static final String TABLE_NAME = "kullanıcılar";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_FIRST_NAME = "first_name";
    private static final String COLUMN_LAST_NAME = "last_name";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_PASSWORD = "pass";

    SQLiteDatabase db;

    private static final String TABLE_CREATE = "CREATE TABLE " + TABLE_NAME + "(" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_FIRST_NAME + " TEXT NOT NULL," +
            COLUMN_LAST_NAME + " TEXT NOT NULL," +
            COLUMN_EMAIL + " TEXT NOT NULL," +
            COLUMN_PASSWORD + " TEXT NOT NULL" +
            ")";


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
        this.db = db;
    }

    public boolean insertUsers(Users u) {
        Boolean success = false;

        try {
            Log.d("helper",u.getEmail()+u.getPassword());
            db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_FIRST_NAME, u.getFirstName());
            cv.put(COLUMN_LAST_NAME, u.getLastName());
            cv.put(COLUMN_EMAIL, u.getEmail());
            cv.put(COLUMN_PASSWORD, u.getPassword());

            db.insert(TABLE_NAME, null, cv);
            db.close();
            success = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return success;
    }

    public boolean isUserLoggedIn() {
        db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Log.d("helper", query);
        boolean success = false;
        try {
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.getCount() <= 0) {
                return success;
            } else {
                success = true;
            }
        } catch (Exception e) {
            Log.d("helper", e.getMessage());
        }
        return success;
    }

    public boolean searchUser(String email, String pass) {

        db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_EMAIL + " = '" + email + "' AND " +
                COLUMN_PASSWORD + " = '" + pass + "'";
        Log.d("helper", email + pass);
        Log.d("helper",query);
        boolean success = false;
        try {
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();

            if (cursor.getCount() == 0) {
                return success;
            } else {
                Log.d("helper", cursor.getString(3) + cursor.getString(4));
                success = true;
            }
        } catch (Exception e) {
            Log.d("helper","exception"+ e.getMessage());
        }
        return success;

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS" + TABLE_NAME);
        this.onCreate(db);
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}
