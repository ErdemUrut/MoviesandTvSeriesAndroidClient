package moviesandtvseries.erdem.com.tr.moviesandtvseries;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomAdapter2 extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater = null;
    JSONArray jsonArray;

    public CustomAdapter2(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return jsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView nameTextView, ratingTextView, genreTextView, runtimeTextView, languageTextView, typeTextView, statusTextView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_tvseries_item, null);

        holder.nameTextView = (TextView) rowView.findViewById(R.id.nameTextView);
        holder.ratingTextView = (TextView) rowView.findViewById(R.id.ratingTextView);
        holder.genreTextView = (TextView) rowView.findViewById(R.id.genreTextView);
        holder.runtimeTextView = (TextView) rowView.findViewById(R.id.runtimeTextView);
        holder.languageTextView = (TextView) rowView.findViewById(R.id.languageTextView);
        holder.typeTextView = (TextView) rowView.findViewById(R.id.typeTextView);
        holder.statusTextView = (TextView) rowView.findViewById(R.id.statusTextView);


        try {
            JSONObject jsonObject2 = jsonArray.getJSONObject(position);
            String name = jsonObject2.getString("name");
            String rating = jsonObject2.getString("rating");
            String genres = jsonObject2.getString("genres");
            String runtime = jsonObject2.getString("runtime");
            String language = jsonObject2.getString("language");
            String type = jsonObject2.getString("type");
            String status = jsonObject2.getString("status");


            holder.nameTextView.setText(name);
            holder.ratingTextView.setText(rating);
            holder.genreTextView.setText(genres);
            holder.runtimeTextView.setText(runtime);
            holder.languageTextView.setText(language);
            holder.typeTextView.setText(type);
            holder.statusTextView.setText(status);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked ", Toast.LENGTH_LONG).show();
            }
        });
        return rowView;
    }

}

