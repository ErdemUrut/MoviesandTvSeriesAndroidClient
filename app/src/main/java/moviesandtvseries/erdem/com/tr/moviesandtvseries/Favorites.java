package moviesandtvseries.erdem.com.tr.moviesandtvseries;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import moviesandtvseries.erdem.com.tr.moviesandtvseries.helpers.JSONParser;

public class Favorites extends AppCompatActivity {

    ArrayList<String> checkedValue;
    ProgressDialog progress;
    ListView lv;
    Movies m = new Movies();

    JSONObject jsonObject = null;
    JSONParser jsonParser = new JSONParser();
    JSONArray jsonArray = new JSONArray();
    CustomAdapter adapter = null;
    String[] favoriteMovies = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        new FavoritesListing().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favorites, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class FavoritesListing extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(Favorites.this);
            progress.setMax(100);
            progress.setMessage("Lütfen bekleyiniz.");
            progress.setTitle("Liste dolduruluyor .");
            progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            jsonObject = jsonParser.makeHttpRequest("http://192.168.2.73:8080/service/movies/favorites/get", "GET", new HashMap<String, String>());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.dismiss();
            try {
                jsonArray = jsonObject.getJSONArray("favoritemovies");
                lv = (ListView) findViewById(R.id.listView2);
                favoriteMovies = new String[jsonArray.length()];
                for(int i = 0;i<jsonArray.length(); i++){
                    favoriteMovies[i] = jsonArray.getJSONObject(i).getString("title");
                }
                lv.setAdapter(new ArrayAdapter<String>(Favorites.this, android.R.layout.simple_list_item_1, favoriteMovies));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
