package moviesandtvseries.erdem.com.tr.moviesandtvseries;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import moviesandtvseries.erdem.com.tr.moviesandtvseries.helpers.JSONParser;

public class RegisterActivity extends AppCompatActivity {

    @Bind(R.id.firstNameEditText)
    EditText firstNameEditText;
    @Bind(R.id.lastNameEditText)
    EditText lastNameEditText;
    @Bind(R.id.emailEditText)
    EditText emailEditText;
    @Bind(R.id.passwordEditText)
    EditText passwordEditText;
    @Bind(R.id.registerButton)
    Button registerButton;
    @Bind(R.id.loginTextView)
    TextView loginTextView;
    Users u = new Users();
    ProgressDialog progress;
    @Bind(R.id.changePasswordTextView)
    TextView changePasswordTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (firstNameEditText.getText().length() <= 0) {
                    Toast.makeText(RegisterActivity.this, "Lütfen adınızı giriniz.", Toast.LENGTH_LONG).show();
                    firstNameEditText.setError("Doldurulması gerekli alan.");
                } else if (lastNameEditText.getText().length() <= 0) {
                    Toast.makeText(RegisterActivity.this, "Lütfen soyadınızı giriniz.", Toast.LENGTH_LONG).show();
                    lastNameEditText.setError("Doldurulması gerekli alan.");
                } else if (emailEditText.getText().length() <= 0) {
                    Toast.makeText(RegisterActivity.this, "Lütfen e-mail adresinizi giriniz.", Toast.LENGTH_LONG).show();
                    emailEditText.setError("Doldurulması gerekli alan.");
                } else if (passwordEditText.getText().length() <= 0) {
                    Toast.makeText(RegisterActivity.this, "Lütfen şifrenizi giriniz.", Toast.LENGTH_LONG).show();
                    passwordEditText.setError("Doldurulması gerekli alan.");
                } else {
                    progress = new ProgressDialog(RegisterActivity.this);
                    progress.setMax(100);
                    progress.setMessage("Lütfen bekleyiniz.");
                    progress.setTitle("Kaydınız yapılıyor .");
                    progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progress.show();
                    new Register().execute();
                }
            }
        });

        loginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        changePasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class Register extends AsyncTask<Void, Void, Void> {

        String strFistName = firstNameEditText.getText().toString();
        String strLastName = lastNameEditText.getText().toString();
        String strEmail = emailEditText.getText().toString();
        String strPassword = passwordEditText.getText().toString();

        JSONObject jsonObject = null;
        Boolean success = null;
        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.dismiss();

            try {
                success = jsonObject.getBoolean("success");
                message = jsonObject.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //success = helper.insertUsers(u);


            if (success) {
                Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_LONG).show();
                startActivity(new Intent(RegisterActivity.this, MainActivity.class));
            } else {
                Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_LONG).show();
            }

        }

        @Override
        protected Void doInBackground(Void... params) {

            JSONParser jsonParser = new JSONParser();
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("email", strEmail);
            parameters.put("first_name", strFistName);
            parameters.put("last_name", strLastName);
            parameters.put("password", strPassword);

            jsonObject = jsonParser.makeHttpRequest("http://dizivefilmtakipws.elasticbeanstalk.com/service/users/register", "POST", parameters);


            return null;
        }
    }
}
