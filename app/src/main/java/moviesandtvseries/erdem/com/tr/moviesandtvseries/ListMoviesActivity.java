package moviesandtvseries.erdem.com.tr.moviesandtvseries;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import moviesandtvseries.erdem.com.tr.moviesandtvseries.helpers.JSONParser;

public class ListMoviesActivity extends AppCompatActivity {

    ArrayList<String> checkedValue;
    ProgressDialog progress;
    ListView lv;
    Movies m = new Movies();
    Button bt1;
    JSONObject jsonObject = null;
    JSONParser jsonParser = new JSONParser();
    JSONArray jsonArray = new JSONArray();
    CustomAdapter adapter = null;
    List<String> moviesToBeSent;
    Button addFavoriteView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_movies);
        new Listing().execute();
        addFavoriteView = (Button) findViewById(R.id.add_favorite);
        addFavoriteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moviesToBeSent = adapter.getCheckedList();
                Log.d("ListMoviesActivity", moviesToBeSent.toString());
                new CheckedItems().execute();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_tv_series, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class Listing extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(ListMoviesActivity.this);
            progress.setMax(100);
            progress.setMessage("Lütfen bekleyiniz.");
            progress.setTitle("Liste dolduruluyor .");
            progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progress.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.dismiss();
            try {
                jsonArray = jsonObject.getJSONArray("movies");
                adapter = new CustomAdapter(ListMoviesActivity.this, jsonArray);
                lv = (ListView) findViewById(R.id.listView);
                lv.setAdapter(adapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            jsonObject = jsonParser.makeHttpRequest("http://192.168.2.73:8080/service/movies", "GET", new HashMap<String, String>());
            return null;
        }
    }

    class CheckedItems extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(ListMoviesActivity.this);
            progress.setMax(100);
            progress.setMessage("Lütfen bekleyiniz.");
            progress.setTitle("Liste dolduruluyor .");
            progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progress.show();
        }

        Boolean success = null;
        String message = "";


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.dismiss();

            try {
                success = jsonObject.getBoolean("success");
                message = jsonObject.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //success = helper.insertUsers(u);


            if (success) {
                Toast.makeText(ListMoviesActivity.this, message, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(ListMoviesActivity.this, message, Toast.LENGTH_LONG).show();
            }


        }

        @Override
        protected Void doInBackground(Void... params) {

            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("movies_id", moviesToBeSent.toString());

            jsonObject = jsonParser.makeHttpRequest("http://192.168.2.73:8080/service/movies/favorites/add", "POST", parameters);

            return null;
        }

    }
}
